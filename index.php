<?php 
// node-red    :: http://35.247.242.59/node-red

// mqtt        :: user: iot pass: deaw1234 topic: sensor
// json format :: {"station_id":1,"PT":32.2,"TT":1.34,"Load_cell":22.4,"Gas_Gun":6.6}

// web         :: http://35.228.187.2
// api & mondo :: www.energicaiot.com
// grafana     :: http://35.247.242.59/grafana
// influx      :: 35.247.242.59
// FT : m3/s.
// PT : psi
// TT : C
// Load Cell : Kg.
// Gas Gun : psi
@session_start();
  if (isset($_SESSION['username'])){
    // $_SESSION['type'] = "Admin";
    // $_SESSION['station_id'] = 1;
    // $_SESSION['username'] = "ADMIN_PTT";
  }else{
    // header("location: login.php");
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <!-- <link rel="icon" type="image/png" href="images/icons/logo-pttep.ico"/> -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Nauticomm</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" type="text/css" href="engine0/style.css" />
  <!-- button toggle -->
  <link rel="stylesheet" type="text/css" href="button-toggle/css/bootstrap2-toggle.min.css" />
  <!-- <link rel="stylesheet" type="text/css" href="button-toggle/css/bootstrap2-toggle.css  " /> -->
  <!-- Font Awesome -->
  <link href="./font/fontawesome/css/all.min.css" rel="stylesheet">

  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
  <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
  
  <!-- google font -->
  <link href ="https://fonts.googleapis.com/css?family=Kanit|Noto+Sans&display=swap" rel="stylesheet">
</head> 
<style>
  * {
    font-family: 'Kanit', sans-serif;
    font-style: Thin;
  }
  body {
    font-family: 'Kanit', sans-serif;
    /* font-size: 16px; */
    /* background-image: url('./data0/images/Udon09.png'); */
    /* background-size: 100%; */
    /* background-repeat: no-repeat; */
    /* padding-top: 8%; */
  }
  #map-and-img-1 {
    padding: 0px 10px 0px 0px;
  }
  #map-and-img-2 {
    padding: 0px 0px 0px 10px;
  }
  @media only screen and (max-width: 960px) {
    /* For mobile phones: */
    #map-and-img-1 {
      padding: 0px 0px 10px 0px;
    }
    #map-and-img-2 {
      padding: 10px 0px 0px 0px;
    }
  }
  @media only screen and (max-width: 700px) {
    /* For mobile phones: */
    .content{
      padding-top: 80px;
    }
    .linebox{
      border-top: solid 2px #1e9fbd;
      width: 100% !important;
    }
  }
  @media only screen and (max-width: 774px) {
    /* For mobile phones: */
    .linebox{
      border-top: solid 2px #1e9fbd;
      width: 100% !important;
    }
  }
  @media only screen and (max-width: 460px) {
    /* For mobile phones: */
    #pro_name{
      font-size: 1.7em !important;
      padding-left: 0px !important;
    }
  }
  .linebox{
    border-top: solid 2px #1e9fbd;
    width: 55%;
  }
  .switch {
    position: relative;
    display: inline-block;
    width: 60px;
    height: 34px;
  }

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #a31640;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

  </style>
<body class="sidebar-mini wysihtml5-supported fixed skin-blue">
<div style="font-weight:400;background-color: #f2f2f2;">


    <header class="main-header">
      <!-- Logo -->
      <a href="#" class="logo" style = "background-color:#34b2cf;">
        <span class="logo-mini" ><b><img src="https://www.nauticomm.com/wp-content/uploads/2015/09/logo.png" width="100px" height="50px"></b></span>
        <span class="logo-lg"><b><img src="https://www.nauticomm.com/wp-content/uploads/2015/09/logo.png" width="100px" height="50px"></b></span>
      </a>
      <nav class="navbar navbar-static-top" style = "background-color:#34b2cf;opacity: 10;">
      </nav>
    </header>
      
      <div class='box box-success'>
        <div class='box-header'>
        <i class="fa fa-star"></i>
          <h1 class='box-title' style = " font-size: 1.8em;padding-right:2%;">
            <strong id = 'hotel_name'></strong>
          </h1>
        </div>
      </div>
    <!-- </section> -->



        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <!-- interactive chart -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <i class="fa fa-th-large"></i>
                  <h3 class="box-title">Control Panel : [ Node-red ]</h3>

                </div>
                <div class="box-body">
                  <!-- <div style="height: 300px;"></div> -->
                  <section class="col-lg-12" >
              <div class='row'>

              <div class="col-lg-3 col-xs-12">
                  <div class="small-box bg-aqua">
                    <div class="inner" style = "height:122px;">
                      <h3 style = "padding-left: 20px;font-size: 40px;color: #76f291;font-size: 2.5em;" id = "pro_name" ></h3>
                      <div class="linebox"></div>
                      <p style = "font-size : 30px;">Process Name.</p>
                      <div style = "font-size: 40px;">
                      </div>
                    </div>
                    <div class="icon" style = "color: #eaf2f4;">
                      <i class="fas fa-cogs"></i>
                    </div>
                    <div href="#" class="small-box-footer"></div>
                  </div>
                </div>


                <div class="col-lg-3 col-xs-12">
                  <div class="small-box bg-aqua">
                    <div class="inner" style = "height:122px;">
                      <h3 style = "padding-left: 20px;font-size: 40px;" id = "mem_u" ></h3>
                      <div class="linebox"></div>
                      <p style = "font-size : 30px;">Memory Usage.</p>
                      <div style = "font-size: 40px;">
                      </div>
                    </div>
                    <div class="icon" style = "color: #eaf2f4;">
                      <i class="fas fa-memory"></i>
                    </div>
                    <div href="#" class="small-box-footer"></div>
                  </div>
                </div>

                <div class="col-lg-3 col-xs-12">
                  <!-- small box -->
                  <div class="small-box bg-aqua">
                    <div class="inner" style = "height:122px;">
                      <h3 style = "padding-left: 20px;font-size: 40px;" id = "cpu_u" ></h3>
                      <div class="linebox"></div>
                      <p style = "font-size : 30px;">CPU Usage.</p>
                      <div style = "font-size: 40px;">
                        <!-- <span id="mousespeed">Loading..</span> -->
                      </div>
                    </div>
                    <div class="icon" style = "color: #eaf2f4;">
                      <i class="fas fa-microchip"></i>
                    </div>
                    <div href="#" class="small-box-footer"></div>
                  </div>
                </div>



              </div>
            </section>
                </div>
                <!-- /.box-body-->
              </div>
              <!-- /.box -->

            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->

      <div class="row">
        <div class="col-md-6">
          <!-- Line chart -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-th-large"></i>
              <h3 class="box-title">Control Panel</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div id="line-chart" style="height: 300px;"></div>
            </div>
            <!-- /.box-body-->
          </div>
          <!-- /.box -->

          <!-- Area chart -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-th-large"></i>
              <h3 class="box-title">Control Panel</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div id="area-chart" style="height: 338px;" class="full-width-chart"></div>
            </div>
            <!-- /.box-body-->
          </div>
          <!-- /.box -->

        </div>
        <div class="col-md-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-th-large"></i>
              <h3 class="box-title">Control Panel</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div id="bar-chart" style="height: 300px;"></div>
            </div>
          </div>
          <!-- /.box -->

          <!-- Donut chart -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-th-large"></i>
              <h3 class="box-title">Control Panel</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div id="donut-chart" style="height: 300px;"></div>
            </div>
            <!-- /.box-body-->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>



      <!-- </div> -->
    <footer class="main-footer">
      <strong>DEAWARE TEAM</strong>
      <div class="pull-right hidden-xs">
        <strong>Nauticomm Web Monitoring</strong>
      </div>
    </footer>
    <div class="control-sidebar-bg"></div>

</div>

<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Sparkline -->
<script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- <script type="text/javascript" src="engine0/wowslider.js"></script>
<script type="text/javascript" src="engine0/script.js"></script> -->
<!-- button toggle -->
<script type="text/javascript" src="button-toggle/js/bootstrap2-toggle.min.js"></script>
<!-- <script type="text/javascript" src="button-toggle/js/bootstrap2-toggle.js"></script> -->
<!-- Google Maps -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB_5EUIpqkV5r2TAhksHU_Mu_mbZ4lxtpI&callback=initMap" async defer></script>
<!-- underscore javascript -->
<!-- fontawesome -->
<script type="text/javascript" src="./font/fontawesome/js/all.min.js"></script>

<script type="text/javascript" src="dist/js/underscore-min.js"></script>
<script language="JavaScript" >
  var time = 6;
  var map,table_s,marker;
  var obj;
  var user_type = "<?php echo $_SESSION['type'];?>";
  $( document ).ready(function() {
    // api_station_param();
    try {
      // api_station_param();
      first_load();
      // $("#vOut1").bootstrapToggle('disable');
    } catch (e) {
      location.reload();
    }
    setInterval(function(){ api_station_param();  }, 1*1000);
    // setInterval(function(){ request_param(); }, 1000);
    // $('#vOut1').bootstrapToggle({on: 'Enabled',off: 'Disabled'});
    // $('#chb1').change(function() {
    //   console.log($(this).prop('checked'));
    // })
  });
function initMap(location) {
  map = new google.maps.Map(document.getElementById('map'), {
    center: location,
    zoom: 16,
    mapTypeId:  google.maps.MapTypeId.HYBRID 
  });
  var myLatLng = location;

  var image = {				
    url:'./images/mark.png',
  };

  var content_in_map = [
    '<strong>แหล่งผลิตน้ำมันดิบอู่ทองโครงการพีทีทีอีพี 1</strong>',
  ];
  bounds = new google.maps.LatLngBounds();

  marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    title: "ที่ตั้งแหล่งผลิตน้ำมัน",
    icon: image,
  });

  bounds.extend(marker.position);

  info = new google.maps.InfoWindow({
    content: content_in_map[<?php echo $_SESSION['station_id']; ?> - 1]
  });
  
  // markers.push(marker);
  
  google.maps.event.addListener(marker, 'click', function() {
    info.open(map, marker);
  });
}

function api_station_param(){
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "./api/param_station.php",
    "method": "GET",
    "headers": {
      "cache-control": "no-cache",
    }
  };

  $.ajax(settings).done(function (response) {
    obj = JSON.parse(response);
    console.log(obj);
    draw_param = obj;
    drawMouseSpeedDemo();
 });
} 

function first_load(){
  var settings = {
    "async": true,
    "crossDomain": true,
    "url": "./api/param_station.php",
    "method": "GET",
    "headers": {
      "cache-control": "no-cache",
    }
  };

  $.ajax(settings).done(function (response) {
    obj = JSON.parse(response);
    draw_param = obj;
    drawMouseSpeedDemo();
  });
}

var draw_param = {};
var pps = 0;
var mpoints_max = 165;
function drawMouseSpeedDemo() {
    var mrefreshinterval = 100; // update display every 500ms
    var lastmousex = -1;
    var lastmousey = -1;
    var lastmousetime;
    var mousetravel = 0;
    var mpoints = [];
    $('html').mousemove(function (e) {
      var mousex = e.pageX;
      var mousey = e.pageY;
      if (lastmousex > -1) {
        mousetravel += Math.max(Math.abs(mousex - lastmousex), Math.abs(mousey - lastmousey));
      }
      lastmousex = mousex;
      lastmousey = mousey;
    });
    var mdraw = function () {
      var md = new Date();
      var timenow = md.getTime();
      if (lastmousetime && lastmousetime != timenow) {
        if (draw_param[0].split(" ")[0] != ""){
          // pps = draw_param[0];
          // console.log($(window).width()); // 758
          // var temp  = Math.round(165 + (165 * (($(window).width() - 758) * 100) / 758) / 100 - 10);
          // console.log(temp);
          // $("#mem_u").html("");
          // $('#mem_u').append((draw_param[62] / 1000000).toFixed(2) + '<sup style="font-size: 20px">GB.</sup>');
          // $("#cpu_u").html("");
          // $('#cpu_u').append(draw_param[37] + '<sup style="font-size: 20px">us.</sup>');
          // [
          //     "top - 03:36:31 up 12 days, 19:23,  1 user,  load average: 0.00, 0.02, 0.00",
          //     "Tasks:   1 total,   0 running,   1 sleeping,   0 stopped,   0 zombie",
          //     "%Cpu(s):  0.4 us,  0.2 sy,  0.0 ni, 99.3 id,  0.1 wa,  0.0 hi,  0.0 si,  0.0 st",
          //     "KiB Mem :  3781800 total,   528448 free,   540456 used,  2712896 buff/cache",
          //     "KiB Swap:        0 total,        0 free,        0 used.  2950948 avail Mem ",
          //     "",
          //     "  PID USER      PR  NI    VIRT    RES    SHR S %CPU %MEM     TIME+ COMMAND",
          //     " 5397 root      20   0 1285616 122540  24024 S  0.0  3.2   0:10.73 node-red",
          //     ""
          // ]
          console.log("------------------------------");
          // console.log(draw_param[0].split(' ').join(''));

          // console.log(draw_param[7].split(" "));
          var arr_1 = draw_param[1].split(" ");
          var arr_2 = draw_param[2].split(" ");
          var arr_3 = draw_param[3].split(" ");
          var arr_4 = draw_param[4].split(" ");
          var arr_5 = draw_param[7].split(" ");
          // console.log(draw_param);
          var arr_sum = [];
          for (var i = 0;i < draw_param.length;i++){
            if (draw_param[i] != ""){
              // console.log(draw_param[i].split(" ").length);
              for (var m = 0;m < draw_param[i].split(" ").length;m++){
                if (draw_param[i].split(" ")[m] != ""){
                  arr_sum.push(draw_param[i].split(" ")[m]);
                  // console.log(draw_param[i].split(" ")[m]);
                }
              }
            }
          }
          // var arr_sum = [];
          // for (var i = 0;i < arr_1.length;i++){
          //   if (arr_1[i] != ""){
          //     arr_sum.push(arr_1[i]);
          //   }
          // }
          // for (var i = 0;i < arr_2.length;i++){
          //   if (arr_2[i] != ""){
          //     arr_sum.push(arr_2[i]);
          //   }
          // }
          // for (var i = 0;i < arr_3.length;i++){
          //   if (arr_3[i] != ""){
          //     arr_sum.push(arr_3[i]);
          //   }
          // }
          // for (var i = 0;i < arr_4.length;i++){
          //   if (arr_4[i] != ""){
          //     arr_sum.push(arr_4[i]);
          //   }
          // }
          // for (var i = 0;i < arr_5.length;i++){
          //   if (arr_5[i] != ""){
          //     arr_sum.push(arr_5[i]);
          //   }
          // }
          console.log(arr_sum);
          console.log("------------------------------");
          $("#mem_u").html("");
          $('#mem_u').append(arr_sum[85] + '<sup style="font-size: 20px">%</sup>');
          $("#cpu_u").html("");
          $('#cpu_u').append(arr_sum[26] + '<sup style="font-size: 20px">us.</sup>');

          $("#pro_name").html("");
          $('#pro_name').append(arr_sum[87]);
        }
        if (mpoints.length > mpoints_max)
          mpoints.splice(0, 1);
        mousetravel = 0;
        $('#mousespeed').sparkline(mpoints, {width: mpoints.length * 2, tooltipSuffix: ' MB.'});
      }
      lastmousetime = timenow;
      setTimeout(mdraw, mrefreshinterval);
    };
    setTimeout(mdraw, mrefreshinterval);
  }
</script>
</body>
</html>
